import { TestBed } from '@angular/core/testing';

import { MoveBlockService } from './move-block.service';

describe('MoveBlockService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MoveBlockService = TestBed.get(MoveBlockService);
    expect(service).toBeTruthy();
  });
});
