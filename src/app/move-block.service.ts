import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoveBlockService {
  private moveSubject = new Subject<any>();
  private moveBlockIndexSubject = new Subject<any>();

  constructor() {
  }

  public setMousePosition = (x: number, y: number, index: number): void => {
    this.moveSubject.next({x, y, index});
  };

  public getMousePosition = (): Observable<any> => this.moveSubject.asObservable();

  public setMoveBlockIndex = (index: number, isDraggble: boolean): void => {
    this.moveBlockIndexSubject.next({index, isDraggble});
  };

  public getMoveBlockIndex = (): Observable<any> => this.moveBlockIndexSubject.asObservable();
}
