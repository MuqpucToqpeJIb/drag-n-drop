import { Component, ViewChild, ElementRef} from '@angular/core';
import * as html2canvas from 'html2canvas';
import { DrawZoneComponent } from './draw-zone/draw-zone.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(DrawZoneComponent, {static: false}) myCanvas: DrawZoneComponent;
  @ViewChild('image', {static: false}) image: ElementRef;
  @ViewChild('imageZone', {static: false}) imageZone: ElementRef;
  public blockIndex: number = 0;

  getSnapShot = ():void => {
    (html2canvas as any)(this.myCanvas.drawZone.nativeElement).then((img)=> {
      this.imageZone.nativeElement.appendChild(img)
    })
  }

  addBlock = (): void => {
    this.myCanvas.addBlock(this.blockIndex);
    this.blockIndex++;
  }

  clearBlock = (): void => {
    this.myCanvas.clearBlock();
  }
}
