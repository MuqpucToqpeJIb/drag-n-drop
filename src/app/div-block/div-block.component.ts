import { Component, OnInit, Input } from '@angular/core';

import {MoveBlockService} from '../move-block.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-div-block',
  templateUrl: './div-block.component.html',
  styleUrls: ['./div-block.component.scss']
})
export class DivBlockComponent implements OnInit {
  @Input() index: number;
  moveBlockSubscribtion: Subscription;
  constructor(private _moveBlockService: MoveBlockService) {
    this.moveBlockSubscribtion = this._moveBlockService.getMousePosition().subscribe(sub => {
      if (sub.index === this.index && this.isDraggble) {
          this.moveBlock(sub.x, sub.y);
      }
      if (sub.index === this.index && this.resize) {
          this.resizeBlock(sub.x, sub.y);
          console.log('sad')
      }
    })
  }
  isToolTipShown = false;
  isDraggble=false;
  toolTipX: string;
  toolTipY: string;
  blockX: string;
  blockY: string;
  clientX: number;
  clientY: number;
  width = '50';
  height = '50';
  resize = false;
  ngOnInit() {
  }

  moveBlock = (x: number, y: number): void => {
    if (this.isDraggble) {
      this.blockX = `${(x - 3)}px`;
      this.blockY = `${(y -3 + window.scrollY)}px`
      this.toolTipX = `${(x + 20)}px`;
      this.toolTipY = `${(y + 20 + window.scrollY)}px`
    }
  }

  setIndexForDrag = () => {
    this._moveBlockService.setMoveBlockIndex(this.index, true);
  }

  setIndexForResize = (event) => {
    this.clientX = event.clientX;
    this.clientY = event.clientY
    this._moveBlockService.setMoveBlockIndex(this.index, true);
  }

  closeToolTip = (): void => {
    this.isToolTipShown = false;
  }

  resizeBlock = (x: number, y: number) => {
    console.log('sadasas', {x, xx: this.clientX})
    if (this.resize && x > this.clientX) {
      this.width = `${(x - this.clientX + 50)}px`;
      this.toolTipX = `${x - 3}px`;
    }
    if (this.resize && y > this.clientY) {
      this.height = `${(y - this.clientY + 50)}px`;
      this.toolTipX = `${y - 3}px`;
    }
  }

  setClientPosition = (event: MouseEvent) => {
    this.clientX = event.clientX;
    this.clientY = event.clientY
  }



}
