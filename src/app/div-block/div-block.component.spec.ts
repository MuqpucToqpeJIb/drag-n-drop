import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivBlockComponent } from './div-block.component';

describe('DivBlockComponent', () => {
  let component: DivBlockComponent;
  let fixture: ComponentFixture<DivBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
