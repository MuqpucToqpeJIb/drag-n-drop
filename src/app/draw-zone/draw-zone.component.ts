import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef, Inject, ComponentFactoryResolver, TemplateRef } from '@angular/core';
import {ImageBlockComponent} from '../image-block/image-block.component';
import {DivBlockComponent} from '../div-block/div-block.component';
import {MoveBlockService} from '../move-block.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-draw-zone',
  templateUrl: './draw-zone.component.html',
  styleUrls: ['./draw-zone.component.scss']
})
export class DrawZoneComponent implements OnInit {
  @ViewChild('innerBlock', {read: ViewContainerRef, static: false}) innerBlock: ViewContainerRef;
  childIndex: any;
  moveBlockSubscription: Subscription;
  constructor(private resolver: ComponentFactoryResolver, private _moveBlockService: MoveBlockService) {
    this.moveBlockSubscription = this._moveBlockService.getMoveBlockIndex().subscribe(sub => {
      console.log(sub);
      this.childIndex = sub.index;

    })
  }
  componentRef: any;
  @ViewChild('drawZone', {static: false}) drawZone: ElementRef;
  
  ngOnInit() {
  }
  addImg = (): void => {
    const factory = this.resolver.resolveComponentFactory(ImageBlockComponent);
    this.componentRef = this.innerBlock.createComponent(factory);
  }

  addBlock = (blockIndex: number): void => {
    const factory = this.resolver.resolveComponentFactory(DivBlockComponent);
    this.componentRef = this.innerBlock.createComponent(factory);
    this.componentRef.instance.index = blockIndex;
  }

  clearBlock = (): void => {
    this.innerBlock.clear();
  }

  onMouseMoveHandler = (event: MouseEvent) => {
    this._moveBlockService.setMousePosition(event.clientX, event.clientY, this.childIndex);
  }
}
