import { BrowserModule, EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DrawZoneComponent } from './draw-zone/draw-zone.component';
import { ImageBlockComponent } from './image-block/image-block.component';
import { DivBlockComponent } from './div-block/div-block.component';

@NgModule({
  declarations: [
    AppComponent,
    DrawZoneComponent,
    ImageBlockComponent,
    DivBlockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ImageBlockComponent, DivBlockComponent]
})
export class AppModule { }
